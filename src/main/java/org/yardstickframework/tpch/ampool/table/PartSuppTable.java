package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class PartSuppTable implements Table{

	private StructType schema = new StructType(new StructField[]{
			new StructField("PS_PARTKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("PS_SUPPKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("PS_AVAILQTY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("PS_SUPPLYCOST", DataTypes.DoubleType,true,Metadata.empty()),
			new StructField("PS_COMMENT", DataTypes.StringType,true,Metadata.empty()),
	});
	
	private String tableName = TableEnum.PARTSUPP.name();
	
	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		primaryKeys.add(schema.fieldNames()[1]);
		return primaryKeys;
	}
	
	@Override
	public StructType getSchema() {
		return schema;
	}

	@Override
	public String getTableName() {
		return tableName;
	}
	
	@Override
	public String partitionColumn(){
		return schema.fieldNames()[0];
	}

}
