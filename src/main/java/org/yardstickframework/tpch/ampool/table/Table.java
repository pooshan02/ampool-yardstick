package org.yardstickframework.tpch.ampool.table;

import java.util.List;

import org.apache.spark.sql.types.StructType;

public interface Table {

	public StructType getSchema();
	
	public String getTableName();
	
	public List<String> getPrimaryKeys();
	
	public String partitionColumn();
}
