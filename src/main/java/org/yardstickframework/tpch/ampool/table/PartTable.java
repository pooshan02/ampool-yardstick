package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class PartTable implements Table{

	private StructType schema = new StructType(new StructField[]{
			new StructField("P_PARTKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("P_NAME", DataTypes.StringType,true,Metadata.empty()),
			new StructField("P_MFGR", DataTypes.StringType,true,Metadata.empty()),
			new StructField("P_BRAND", DataTypes.StringType,true,Metadata.empty()),
			new StructField("P_TYPE", DataTypes.StringType,true,Metadata.empty()),
			new StructField("P_SIZE", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("P_CONTAINER", DataTypes.StringType,true,Metadata.empty()),
			new StructField("P_RETAILPRICE", DataTypes.DoubleType,true,Metadata.empty()),
			new StructField("P_COMMENT", DataTypes.StringType,true,Metadata.empty()),
	});
	
	private String tableName = TableEnum.PART.name();
	
	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		return primaryKeys;
	}
	
	@Override
	public StructType getSchema() {
		return schema;
	}

	@Override
	public String getTableName() {
		return tableName;
	}
	
	@Override
	public String partitionColumn(){
		return schema.fieldNames()[0];
	}

}
