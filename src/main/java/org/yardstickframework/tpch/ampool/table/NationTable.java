package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class NationTable implements Table{

	private StructType schema = new StructType(new StructField[]{
			new StructField("N_NATIONKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("N_NAME", DataTypes.StringType,true,Metadata.empty()),
			new StructField("N_REGIONKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("N_COMMENT", DataTypes.StringType,true,Metadata.empty())
	});
	
	private String tableName = TableEnum.NATION.name();
	
	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		return primaryKeys;
	}

	public StructType getSchema() {
		return schema;
	}

	public String getTableName() {
		return tableName;
	}
	
	@Override
	public String partitionColumn(){
		return schema.fieldNames()[0];
	}

	
}
