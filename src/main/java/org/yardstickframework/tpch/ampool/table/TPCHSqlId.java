/**
 * 
 */
package org.yardstickframework.tpch.ampool.table;

/**
 * @author pooshans
 *
 */
public enum TPCHSqlId {

	SQL_1(1), SQL_2(2), SQL_3(3), SQL_4(4), SQL_5(5), SQL_6(6), SQL_7(7), SQL_8(8), SQL_9(9), SQL_10(10), SQL_11(
			11), SQL_12(12), SQL_13(13), SQL_14(
					14), SQL_15(15), SQL_16(16), SQL_17(17), SQL_18(18), SQL_19(19), SQL_20(20), SQL_21(21), SQL_22(22);

	private int sqlId;

	private TPCHSqlId(int sqlId) {
		this.sqlId = sqlId;
	}

	public int getId() {
		return sqlId;
	}

}
