package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class LineItemTable implements Table {

	 //Kudu recognize only TimestampType for date only. Change to DateType if it does not work;
	
	private StructType schema = new StructType(
			new StructField[] { new StructField("L_ORDERKEY", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("L_PARTKEY", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("L_SUPPKEY", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("L_LINENUMBER", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("L_QUANTITY", DataTypes.DoubleType, true, Metadata.empty()),
					new StructField("L_EXTENDEDPRICE", DataTypes.DoubleType, true, Metadata.empty()),
					new StructField("L_DISCOUNT", DataTypes.DoubleType, true, Metadata.empty()),
					new StructField("L_TAX", DataTypes.DoubleType, true, Metadata.empty()),
					new StructField("L_RETURNFLAG", DataTypes.StringType, true, Metadata.empty()),
					new StructField("L_LINESTATUS", DataTypes.StringType, true, Metadata.empty()),
					new StructField("L_SHIPDATE", DataTypes.TimestampType, true, Metadata.empty()), 
					new StructField("L_COMMITDATE", DataTypes.TimestampType, true, Metadata.empty()),
					new StructField("L_RECEIPTDATE", DataTypes.TimestampType, true, Metadata.empty()),
					new StructField("L_SHIPINSTRUCT", DataTypes.StringType, true, Metadata.empty()),
					new StructField("L_SHIPMODE", DataTypes.StringType, true, Metadata.empty()),
					new StructField("L_COMMENT", DataTypes.StringType, true, Metadata.empty()) });

	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		primaryKeys.add(schema.fieldNames()[3]);
		return primaryKeys;
	}

	private String tableName = TableEnum.LINEITEM.name();

	@Override
	public StructType getSchema() {
		return schema;
	}

	@Override
	public String getTableName() {
		return tableName;
	}
	
	@Override
	public String partitionColumn(){
		return schema.fieldNames()[0];
	}

}
