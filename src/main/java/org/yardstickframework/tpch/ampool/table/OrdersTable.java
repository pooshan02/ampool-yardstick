package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class OrdersTable implements Table{

	//Kudu recognize only TimestampType for date only. Change to DateType if it does not work;
	
	private StructType schema = new StructType(new StructField[]{
			new StructField("O_ORDERKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("O_CUSTKEY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("O_ORDERSTATUS", DataTypes.StringType,true,Metadata.empty()),
			new StructField("O_TOTALPRICE", DataTypes.DoubleType,true,Metadata.empty()),
			new StructField("O_ORDERDATE", DataTypes.TimestampType,true,Metadata.empty()),
			new StructField("O_ORDERPRIORITY", DataTypes.StringType,true,Metadata.empty()),
			new StructField("O_CLERK", DataTypes.StringType,true,Metadata.empty()),
			new StructField("O_SHIPPRIORITY", DataTypes.IntegerType,true,Metadata.empty()),
			new StructField("O_COMMENT", DataTypes.StringType,true,Metadata.empty()),
	});
	
	private String tableName = TableEnum.ORDERS.name();
	
	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		return primaryKeys;
	}
	
	@Override
	public StructType getSchema() {
		return schema;
	}

	@Override
	public String getTableName() {
		return tableName;
	}
	
	@Override
	public String partitionColumn(){
		return schema.fieldNames()[0];
	}

}
