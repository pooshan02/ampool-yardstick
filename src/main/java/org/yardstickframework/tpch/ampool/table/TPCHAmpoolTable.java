package org.yardstickframework.tpch.ampool.table;

import io.ampool.monarch.table.Schema;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

/**
 * @author pooshans
 *
 */
public class TPCHAmpoolTable {

	
	/**
	 * Customer table
	 *
	 */
	public static class Customer {
		public static final String[] columnNames = new String[] { "C_CUSTKEY", "C_NAME", "C_ADDRESS", "C_NATIONKEY",
				"C_PHONE", "C_ACCTBAL", "C_MKTSEGMENT", "C_COMMENT" };
		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.STRING, BasicTypes.INT, BasicTypes.STRING, BasicTypes.DOUBLE, BasicTypes.STRING,
				BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Customer.class.getSimpleName();
		}

	}

	/**
	 * LineItem table
	 *
	 */
	public static class LineItem {
		public static final String[] columnNames = new String[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY",
				"L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS",
				"L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.INT, BasicTypes.INT,
				BasicTypes.INT, BasicTypes.DOUBLE, BasicTypes.DOUBLE, BasicTypes.DOUBLE, BasicTypes.DOUBLE,
				BasicTypes.STRING, BasicTypes.STRING, BasicTypes.DATE, BasicTypes.DATE, BasicTypes.DATE,
				BasicTypes.STRING, BasicTypes.STRING, BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return LineItem.class.getSimpleName();
		}
	}

	/**
	 * Nation table
	 *
	 */
	public static class Nation {
		public static final String[] columnNames = new String[] { "N_NATIONKEY", "N_NAME", "N_REGIONKEY", "N_COMMENT" };
		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING, BasicTypes.INT,
				BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Nation.class.getSimpleName();
		}
	}

	/**
	 * Orders table
	 *
	 */
	public static class Orders {
		public static final String[] columnNames = new String[] { "O_ORDERKEY", "O_CUSTKEY", "O_ORDERSTATUS",
				"O_TOTALPRICE", "O_ORDERDATE", "O_ORDERPRIORITY", "O_CLERK", "O_SHIPPRIORITY", "O_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.DOUBLE, BasicTypes.DATE, BasicTypes.STRING, BasicTypes.STRING, BasicTypes.INT,
				BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Orders.class.getSimpleName();
		}
	}

	/**
	 * Part table
	 *
	 */
	public static class Part {
		public static final String[] columnNames = new String[] { "P_PARTKEY", "P_NAME", "P_MFGR", "P_BRAND", "P_TYPE",
				"P_SIZE", "P_CONTAINER", "P_RETAILPRICE", "P_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.STRING, BasicTypes.STRING, BasicTypes.STRING, BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.DOUBLE, BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Part.class.getSimpleName();
		}
	}

	/**
	 * PartSupp table
	 *
	 */
	public static class PartSupp {
		public static final String[] columnNames = new String[] { "PS_PARTKEY", "PS_SUPPKEY", "PS_AVAILQTY",
				"PS_SUPPLYCOST", "PS_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.INT, BasicTypes.INT,
				BasicTypes.DOUBLE, BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return PartSupp.class.getSimpleName();
		}
	}

	/**
	 * Region table
	 *
	 */
	public static class Region {
		public static final String[] columnNames = new String[] { "R_REGIONKEY", "R_NAME", "R_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.STRING };

		public static Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Region.class.getSimpleName();
		}
	}

	/**
	 * Supplier table
	 *
	 */
	public static class Supplier {
		public static final String[] columnNames = new String[] { "S_SUPPKEY", "S_NAME", "S_ADDRESS", "S_NATIONKEY",
				"S_PHONE", "S_ACCTBAL", "S_COMMENT" };

		public static final DataType[] columnTypes = new DataType[] { BasicTypes.INT, BasicTypes.STRING,
				BasicTypes.STRING, BasicTypes.INT, BasicTypes.STRING, BasicTypes.DOUBLE, BasicTypes.STRING };

		public static  Schema getSchema() {
			return new Schema(columnNames, columnTypes);
		}

		public static String getTableName() {
			return Supplier.class.getSimpleName();
		}
	}

}
