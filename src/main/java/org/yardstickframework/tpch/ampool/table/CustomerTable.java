package org.yardstickframework.tpch.ampool.table;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class CustomerTable implements Table {

	private StructType schema = new StructType(
			new StructField[] { new StructField("C_CUSTKEY", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("C_NAME", DataTypes.StringType, true, Metadata.empty()),
					new StructField("C_ADDRESS", DataTypes.StringType, true, Metadata.empty()),
					new StructField("C_NATIONKEY", DataTypes.IntegerType, true, Metadata.empty()),
					new StructField("C_PHONE", DataTypes.StringType, true, Metadata.empty()),
					new StructField("C_ACCTBAL", DataTypes.DoubleType, true, Metadata.empty()),
					new StructField("C_MKTSEGMENT", DataTypes.StringType, true, Metadata.empty()),
					new StructField("C_COMMENT", DataTypes.StringType, true, Metadata.empty()), });

	private String tableName = TableEnum.CUSTOMER.name();

	@Override
	public List<String> getPrimaryKeys() {
		List<String> primaryKeys = new ArrayList<String>();
		primaryKeys.add(schema.fieldNames()[0]);
		return primaryKeys;
	}

	@Override
	public StructType getSchema() {
		return schema;
	}

	@Override
	public String getTableName() {
		return tableName;
	}

	@Override
	public String partitionColumn() {
		return schema.fieldNames()[0];
	}

}
