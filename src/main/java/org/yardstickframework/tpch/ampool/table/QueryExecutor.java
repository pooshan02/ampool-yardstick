package org.yardstickframework.tpch.ampool.table;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * The class QueryExecutor is TCP-H benchmarking compliance program. It is for
 * both Ampool and Hdfs(orc).
 * 
 * @author pooshans
 *
 */
public class QueryExecutor {

	private SparkSession sparkSession;
	private SourceType sourceType;
	private Map<String, String> options;

	public QueryExecutor(SourceType sourceType, SparkSession sparkSession, String host, String port) {
		this.sparkSession = sparkSession;
		this.sourceType = sourceType;
		switch (sourceType) {
		case Ampool:
			this.options = new HashMap<>(3);
			options.put("ampool.locator.host", host);
			options.put("ampool.locator.port", String.valueOf(port));
			break;
		default:
			System.err.print("\n\n****Invalid Source Type!****\n\n");
			break;
		}
	}

	private String sql = null;
	private TPCHSqlId tpcSqlId;
	private int defaultPartitionMarker = 0;

	public void prepare(TPCHSqlId tpcSqlId) {
		prepare(tpcSqlId, defaultPartitionMarker);
	}

	/**
	 * It perform the execution of the TCP-H compliance queries.
	 */
	public void prepare(TPCHSqlId tpcSqlId, int partitions) {
		this.tpcSqlId = tpcSqlId;
		Dataset<Row> dataset = null;
		switch (tpcSqlId) {
		case SQL_1:
			dataset = createOrReplaceTempView(false, false, false, false, false, true, false, false);
			sql = TPCHQuery.sql1;
			break;
		case SQL_2:
			dataset = createOrReplaceTempView(true, true, true, false, false, false, true, true);
			sql = TPCHQuery.sql2;
			break;
		case SQL_3:
			dataset = createOrReplaceTempView(false, false, false, true, true, true, false, false);
			sql = TPCHQuery.sql3;
			break;
		case SQL_4:
			dataset = createOrReplaceTempView(false, false, false, false, true, true, false, false);
			sql = TPCHQuery.sql4;
			break;
		case SQL_5:
			dataset = createOrReplaceTempView(false, true, false, true, true, true, true, true);
			sql = TPCHQuery.sql5;
			break;
		case SQL_6:
			dataset = createOrReplaceTempView(false, false, false, false, false, true, false, false);
			sql = TPCHQuery.sql6;
			break;
		case SQL_7:
			dataset = createOrReplaceTempView(false, true, false, true, true, true, true, false);
			sparkSession.sql(TPCHQuery.subquerySql7).createOrReplaceTempView("shipping");
			sql = TPCHQuery.sqlWithSubquerySql7;
			break;
		case SQL_8:
			dataset = createOrReplaceTempView(true, true, false, true, true, true, true, true);
			sql = TPCHQuery.sql8;
			break;
		case SQL_9:
			dataset = createOrReplaceTempView(true, true, true, false, true, true, true, false);
			sql = TPCHQuery.sql9;
			break;
		case SQL_10:
			dataset = createOrReplaceTempView(false, false, false, true, true, true, true, false);
			sql = TPCHQuery.sql10;
			break;
		case SQL_11:
			dataset = createOrReplaceTempView(false, true, true, false, false, false, true, false);
			sql = TPCHQuery.sql11;
			break;
		case SQL_12:
			dataset = createOrReplaceTempView(false, false, false, false, true, true, false, false);
			sql = TPCHQuery.sql12;
			break;
		case SQL_13:
			dataset = createOrReplaceTempView(false, false, false, true, true, false, false, false);
			sql = TPCHQuery.sql13;
			break;
		case SQL_14:
			dataset = createOrReplaceTempView(true, false, false, false, false, true, false, false);
			sql = TPCHQuery.sql14;
			break;
		case SQL_15:
			dataset = createOrReplaceTempView(false, true, false, false, false, true, false, false);
			sparkSession.sql(TPCHQuery.sqlView15).createOrReplaceTempView("REVENUE0");
			sql = TPCHQuery.sqlWithView15;
			break;
		case SQL_16:
			dataset = createOrReplaceTempView(true, true, true, false, false, false, false, false);
			sql = TPCHQuery.sql16;
			break;
		case SQL_17:
			dataset = createOrReplaceTempView(true, false, false, false, false, true, false, false);
			sql = TPCHQuery.sql17;
			break;
		case SQL_18:
			dataset = createOrReplaceTempView(false, false, false, true, true, true, false, false);
			sql = TPCHQuery.sql18;
			break;
		case SQL_19:
			dataset = createOrReplaceTempView(true, false, false, false, false, true, false, false);
			sql = TPCHQuery.sql19;
			break;
		case SQL_20:
			dataset = createOrReplaceTempView(true, true, true, false, false, true, true, false);
			sql = TPCHQuery.sql20;
			break;
		case SQL_21:
			dataset = createOrReplaceTempView(false, true, false, false, true, true, true, true);
			sql = TPCHQuery.sql21;
			break;
		case SQL_22:
			dataset = createOrReplaceTempView(false, false, false, true, true, false, false, false);
			sql = TPCHQuery.sql22;
			break;
		default:
			System.err.println("****Invalid tpc-h query id****");
			System.exit(-1);
		}
		Dataset<Row> ds = (partitions > 0) ? dataset.repartition(partitions) : dataset;
	}

	private Dataset<Row> createOrReplaceTempView(boolean isPartTempView, boolean isSupplierTempView,
			boolean isPartsuppTempView, boolean isCustomerTempView, boolean isOrdersTempView,
			boolean isLineItemTempView, boolean isNationTempView, boolean isRegionTempView) {
		Dataset<Row> dataset = null;
		if (isPartTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.PART.name()))
						.createOrReplaceTempView("temp_part");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isSupplierTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.SUPPLIER.name()))
						.createOrReplaceTempView("temp_supplier");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isPartsuppTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.PARTSUPP.name()))
						.createOrReplaceTempView("temp_partSupp");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isCustomerTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.CUSTOMER.name()))
						.createOrReplaceTempView("temp_customer");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isOrdersTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.ORDERS.name()))
						.createOrReplaceTempView("temp_orders");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isLineItemTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.LINEITEM.name()))
						.createOrReplaceTempView("temp_lineItem");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isNationTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.NATION.name()))
						.createOrReplaceTempView("temp_nation");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		if (isRegionTempView) {
			switch (sourceType) {
			case Ampool:
				(dataset = sparkSession.read().format("io.ampool").options(options).load(TableEnum.REGION.name()))
						.createOrReplaceTempView("temp_region");
				break;
			default:
				System.err.print("\n\n****Invalid Source Type!****\n\n");
				break;
			}
		}
		return dataset;
	}

	public void execute(int numberOfIterations) {
		long startTime = 0l;
		long totalTime = 0l;
		int lterations = numberOfIterations;

		while (lterations != 0) {
			startTime = System.currentTimeMillis();
			Dataset<Row> ds = sparkSession.sql(sql);
			try {
				ds.collectAsList();
			} catch (Exception ex) {
				ex.printStackTrace();
				System.err.println("Problem occured in sqlId {" + tpcSqlId + "}");
			}
			long currentTime = System.currentTimeMillis();
			System.out.println("TPC-H Sql Id {" + tpcSqlId + "} time(ms) taken {" + (currentTime - startTime) + "}");
			totalTime = totalTime + (currentTime - startTime);
			lterations--;
		}
		System.out.println(
				"TPC-H Sql Id {" + tpcSqlId + "} avg. time(ms) taken {" + (totalTime / numberOfIterations) + "}");
	}
}
