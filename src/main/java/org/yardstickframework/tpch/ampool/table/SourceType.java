/**
 * 
 */
package org.yardstickframework.tpch.ampool.table;

/**
 * @author pooshans
 *
 */
public enum SourceType {
	Ampool("ampool");

	private String name;

	private SourceType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
