/**
 * 
 */
package org.yardstickframework.tpch.ampool.table;

/**
 * @author pooshans
 *
 */
public enum TableEnum {

	/** The TPC PART Table name **/
	PART("PART"),

	/** The TPC SUPPLIER Table name **/
	SUPPLIER("SUPPLIER"),

	/** The TPC PARTSUPP Table name **/
	PARTSUPP("PARTSUPP"),

	/** The TPC CUSTOMER Table name **/
	CUSTOMER("CUSTOMER"),

	/** The TPC ORDERS Table name **/
	ORDERS("ORDERS"),

	/** The TPC LINEITEM Table name **/
	LINEITEM("LINEITEM"),

	/** The TPC NATION Table name **/
	NATION("NATION"),

	/** The TPC REGION Table name **/
	REGION("REGION");

	private String name;

	private TableEnum(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
