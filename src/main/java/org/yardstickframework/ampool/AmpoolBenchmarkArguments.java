package org.yardstickframework.ampool;

import com.beust.jcommander.Parameter;

public class AmpoolBenchmarkArguments {

	@Parameter(names = {"-lh", "--locatorHost"}, description = "Locator host Name")
	private String locatorHost = "localhost";
	
	@Parameter(names = {"-lp", "--locatorPort"}, description = "Locator port number")
	private int locatorPort = 10334;
	
	@Parameter(names = {"-r", "--range"}, description = "Key range")
    private int range = 1_000_000;
	
	@Parameter(names = {"-bch", "--batchSize"}, description = "Batch size")
    private int batchSize = 1_000;
	
	@Parameter(names = {"-jars", "--jars"}, description = "Colon separated list of third party Jars to run spark jobs")
	private String dependencyJars = "";
	
	@Parameter(names = {"-appName", "--appName"}, description = "Spark Application Name")
	private String appName;
	
	@Parameter(names = {"-dh", "--driverHost"}, description = "Spark driver hostname")
	private String driverHost;
	
	@Parameter(names = {"-spm", "--sparkMaster"}, description = "Spark Master URL")
	private String sparkMaster;
	
	public String locatorHost(){
		return locatorHost;
	}
	
	public int locatorPort(){
		return locatorPort;
	}
	
	public int range() {
        return range;
    }
	
	public int batchSize() {
        return batchSize;
    }
	
	public String[] dependencyJars(){
		return dependencyJars.split(":");
	}
	
	public String appName(){
		return appName;
	}
	
	public String driverHost(){
		return driverHost;
	}
	
	public String sparkMaster(){
		return sparkMaster;
	}
	
}
