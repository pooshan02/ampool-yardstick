package org.yardstickframework.ampool.region;

import static org.yardstickframework.BenchmarkUtils.jcommander;
import static org.yardstickframework.BenchmarkUtils.println;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.BenchmarkDriverAdapter;
import org.yardstickframework.BenchmarkUtils;
import org.yardstickframework.ampool.AmpoolBenchmarkArguments;
import org.yardstickframework.tpch.ampool.table.QueryExecutor;
import org.yardstickframework.tpch.ampool.table.SourceType;

public abstract class AmpoolTpchAbstractBenchmark extends BenchmarkDriverAdapter {
	protected final AmpoolBenchmarkArguments args = new AmpoolBenchmarkArguments();
	protected JavaSparkContext sc;
	protected QueryExecutor executor;

	@Override
	public void setUp(BenchmarkConfiguration cfg) throws Exception {
		super.setUp(cfg);
		jcommander(cfg.commandLineArguments(), args, "<spark-driver>");

		println("****Initialiaing spark****");

		String[] jars = args.dependencyJars();
		println("Jars :: " + Arrays.toString(jars));
		String extraClassPath = StringUtils.join(jars, ":");
		SparkConf conf = new SparkConf().setAppName(args.appName()).set("spark.eventLog.enabled", "true")
				.set("spark.driver.host", args.driverHost()).setJars(jars)
				.set("spark.driver.extraClassPath", extraClassPath).set("spark.executor.extraClassPath", extraClassPath)
				.setMaster(args.sparkMaster());
		sc = new JavaSparkContext(conf);
		SparkSession sparkSession = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
		executor = new QueryExecutor(SourceType.Ampool, sparkSession, args.locatorHost(),
				String.valueOf(args.locatorPort()));
		println("****Completed****");
	}

	@Override
	public String usage() {
		return BenchmarkUtils.usage(args);
	}

}
