package org.yardstickframework.ampool.region;

import java.util.Map;

import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.Put;
import io.ampool.monarch.table.ftable.FTable;

public class AmpoolPutBenchmark extends AmpoolTableAbstractBenchmark{

	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		int key = nextRandom(args.range());
		Put record = new Put(Bytes.toBytes(key));
		record.addColumn(Bytes.toBytes("ID"), Bytes.toBytes(key));
		mtable.put(record);
		return true;
	}

	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}

	
}
