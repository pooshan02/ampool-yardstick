package org.yardstickframework.ampool.region;

import static org.yardstickframework.BenchmarkUtils.println;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.yardstickframework.BenchmarkConfiguration;

import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.ftable.FTable;
import io.ampool.monarch.table.ftable.Record;

public class AmpoolSparkSqlJoinBenchmark extends AmpoolSparkAbstractBenchmark{

	private SQLContext sqlContext;
	
	@Override
	public void setUp(BenchmarkConfiguration cfg) throws Exception{
		super.setUp(cfg);
		
		println(cfg, "Populating query data...");
		
		long start = System.nanoTime();
		
		final int orgRange = args.range() / 10;
		
		FTable org = client.getFTable(orgTable);		
		for (int i = 0; i < orgRange && !Thread.currentThread().isInterrupted(); i++){
			Record record = new Record();
			record.add(columnNamesOrg[0], i);
			record.add(columnNamesOrg[1], "org" + i);
			org.append(record);
		}
		
		FTable person = client.getFTable(personTable);
		for (int i = orgRange; i < orgRange + args.range() && !Thread.currentThread().isInterrupted(); i++){
			Record record = new Record();
			record.add(columnNamesPerson[0], i);
			record.add(columnNamesPerson[1], nextRandom(orgRange));
			record.add(columnNamesPerson[2], "firstName"+i);
			record.add(columnNamesPerson[3], "lastName"+i);
			record.add(columnNamesPerson[4], (i - orgRange) * 1000.0);
			person.append(record);
			
			if (i % 100000 == 0)
	            println(cfg, "Populated persons: " + i);
		}
		
		println(cfg, "Finished populating join query data in " + ((System.nanoTime() - start) / 1_000_000) + " ms.");
		
		sqlContext = new SQLContext(sc);
		
		Map<String, String> options = new HashMap<>(3);
	    options.put("ampool.locator.host", locatorHost);
	    options.put("ampool.locator.port", String.valueOf(locatorPort));
	    
	    Dataset personDF = sqlContext.read().format("io.ampool").options(options).load(personTable);
	    personDF.registerTempTable("temp_"+personTable);
	    
	    Dataset orgDF = sqlContext.read().format("io.ampool").options(options).load(orgTable);
	    orgDF.registerTempTable("temp_"+orgTable);
	}
	
	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		
		double salary = ThreadLocalRandom.current().nextDouble() * args.range() * 1000;
		double maxSalary = salary + 1000;
		
		Collection<Row> entries = executeQueryJoin(salary, maxSalary);
		
		for (Row row : entries) {
            double sal = row.getDouble(4);

            if (sal < salary || sal > maxSalary) {
                throw new Exception("Invalid Employee retrieved [min=" + salary + ", max=" + maxSalary +
                    ", EmpId=" + row.getInt(1) + ']');
            }
        }
		
		return true;
	}

	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}
	
	public Collection<Row> executeQueryJoin(double salary, double maxSalary){
		String sql = "SELECT p.ID,p.ORGID,p.FIRSTNAME,p.LASTNAME,p.SALARY,o.NAME "
				+ "FROM " + "temp_"+personTable+" as p"
				+ " LEFT OUTER JOIN " + "temp_"+orgTable+" as o"
				+ " ON p.ID = o.ID "
				+ "WHERE SALARY >= " + salary + " AND SALARY <= "+ maxSalary;
		
		return sqlContext.sql(sql).collectAsList();
	}

}
