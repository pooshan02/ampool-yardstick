package org.yardstickframework.ampool.region;

import static org.yardstickframework.BenchmarkUtils.println;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.BenchmarkUtils;

public abstract class AmpoolSparkAbstractBenchmark extends AmpoolTableAbstractBenchmark{

	protected JavaSparkContext sc;
	
	@Override
	public void setUp(BenchmarkConfiguration cfg) throws Exception{
		super.setUp(cfg);
		
		File logFolder = new File("/home/talentica/spark-events");
		
		if (!logFolder.exists()) {
            boolean mkdir = logFolder.mkdir();

            println(cfg, "Log directory created. " + logFolder.getAbsolutePath());
        }
		String[] jars = args.dependencyJars();
		
		String extraClassPath = StringUtils.join(jars, ":");
		
		SparkConf conf = new SparkConf().setAppName(args.appName())
				.set("spark.eventLog.enabled", "true")
				.set("spark.driver.host", args.driverHost())
				.setJars(jars)
				.set("spark.driver.extraClassPath",extraClassPath)
				.set("spark.executor.extraClassPath",extraClassPath)
				.setMaster(args.sparkMaster());
		
		sc = new JavaSparkContext(conf);
	}
	
	@Override
	public void tearDown(){
		if(sc != null)
			sc.stop();
	}
	
	@Override
	public String usage(){
		return BenchmarkUtils.usage(args);
	}
	
	protected int nextRandom(int max) {
        return ThreadLocalRandom.current().nextInt(max);
    }
}
