package org.yardstickframework.ampool.region;

import java.util.Map;

import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.tpch.ampool.table.TPCHSqlId;

public class AmpoolTpchSparkSql8Benchmark extends AmpoolTpchAbstractBenchmark {

	public void setUp(BenchmarkConfiguration cfg) throws Exception {
		super.setUp(cfg);
		executor.prepare(TPCHSqlId.SQL_8);
	}

	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		executor.execute(1);
		return true;
	}

	@Override
	public void tearDown() {
		sc.close();
	}

}
