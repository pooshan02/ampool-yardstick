package org.yardstickframework.ampool.region;

import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.ampool.AmpoolAbstractBenchmark;

import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.ftable.FTable;

public abstract class AmpoolTableAbstractBenchmark extends AmpoolAbstractBenchmark{

	protected FTable ftable;
	protected MTable mtable;
	
	@Override
	public void setUp(BenchmarkConfiguration cfg) throws Exception{
		super.setUp(cfg);
		
		ftable = fTable();
		mtable = mTable();
	}
	
	protected abstract FTable fTable();
	
	protected abstract MTable mTable();
}
