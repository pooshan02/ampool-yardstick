package org.yardstickframework.ampool.region;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.Put;
import io.ampool.monarch.table.ftable.FTable;

public class AmpoolPutAllBenchmark extends AmpoolTableAbstractBenchmark{

	@Override
	public boolean test(Map<Object, Object> ctx) throws Exception {
		int batchSize = args.batchSize();
		
		List<Put> records = new ArrayList<Put>();
		for(int i = 0; i < batchSize; i++){
            int key = nextRandom(args.range());
			Put put = new Put(Bytes.toBytes(key));
			put.addColumn(Bytes.toBytes("ID"), Bytes.toBytes(key));
			records.add(put);
		}
		mtable.put(records);
		/*Record[] records = new Record[batchSize];
		for(int i = 0; i < batchSize; i++){
			Record record = new Record();
			record.add(columnNames[0], "User"+i);
			record.add(columnNames[1], nextRandom(args.range()));
			record.add(columnNames[2], 27);
			record.add(columnNames[3], 45.0);
			record.add(columnNames[4], "D-12");
			record.add(columnNames[5], Date.valueOf("1996-01-02"));
			records[i] = record;
		}
		ftable.append(records);*/
		return true;
	}

	@Override
	protected FTable fTable() {
		return client.getFTable(personTable);
	}

	@Override
	protected MTable mTable() {
		return client.getMTable(mTable);
	}

}
