package org.yardstickframework.ampool;

import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.BenchmarkServer;

public class AmpoolNode implements BenchmarkServer{

	@Override
	public void start(BenchmarkConfiguration cfg) throws Exception {
		System.out.println("Dummy method for Server Started...");
	}

	@Override
	public void stop() throws Exception {
		System.out.println("Dummy method for Server Stop...");
	}

	@Override
	public String usage() {
		return "usage";
	}

}
