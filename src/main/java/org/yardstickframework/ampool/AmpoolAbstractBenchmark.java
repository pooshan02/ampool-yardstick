package org.yardstickframework.ampool;

import static org.yardstickframework.BenchmarkUtils.jcommander;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.yardstickframework.BenchmarkConfiguration;
import org.yardstickframework.BenchmarkDriverAdapter;

import io.ampool.client.AmpoolClient;
import io.ampool.conf.Constants;
import io.ampool.monarch.table.Admin;
import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.MTableDescriptor;
import io.ampool.monarch.table.MTableType;
import io.ampool.monarch.table.Schema;
import io.ampool.monarch.table.ftable.FTableDescriptor;
import io.ampool.monarch.types.BasicTypes;
import io.ampool.monarch.types.interfaces.DataType;

public abstract class AmpoolAbstractBenchmark extends BenchmarkDriverAdapter {
	protected final AmpoolBenchmarkArguments args = new AmpoolBenchmarkArguments();
	protected AmpoolClient client;
	
	protected String locatorHost;
	protected int locatorPort;
	
	protected static final String[] columnNamesPerson = {"ID","ORGID", "FIRSTNAME", "LASTNAME", "SALARY"};
	protected static final String[] columnNamesOrg = {"ID","NAME"};
	protected static final String mTable = "MTableExample";
	protected static final String personTable = "PersonTable";
	protected static final String orgTable = "OrgTable";
	protected static final DataType[] columnTypes = {BasicTypes.INT,BasicTypes.INT,
			BasicTypes.STRING,BasicTypes.STRING,BasicTypes.DOUBLE};
	protected static final DataType[] columnTypesOrg = {BasicTypes.INT,BasicTypes.STRING};
	
	@Override
	public void setUp(BenchmarkConfiguration cfg) throws Exception{
		
		super.setUp(cfg);
		jcommander(cfg.commandLineArguments(), args, "<spark-driver>");
		
		Properties props = new Properties();
		locatorHost = args.locatorHost();
		locatorPort = args.locatorPort();
		
	    props.setProperty(Constants.MClientCacheconfig.MONARCH_CLIENT_LOG, "/tmp/ampoolTable.log");
	    client = new AmpoolClient(locatorHost,locatorPort,props);
	    
	    final Admin admin = client.getAdmin();
	    FTableDescriptor personTableDescriptor = new FTableDescriptor();
	    
	    personTableDescriptor.setSchema(new Schema(columnNamesPerson,columnTypes));
	    personTableDescriptor.setPartitioningColumn(Bytes.toBytes(columnNamesPerson[0]));
	    if (admin.existsFTable(personTable)) {
	        admin.deleteFTable(personTable);
	    }
	    admin.createFTable(personTable, personTableDescriptor);
	    
	    FTableDescriptor orgTableDescriptor = new FTableDescriptor();
	    
	    orgTableDescriptor.setSchema(new Schema(columnNamesOrg,columnTypesOrg));
	    orgTableDescriptor.setPartitioningColumn(Bytes.toBytes(columnNamesOrg[0]));
	    if(admin.existsFTable(orgTable)){
	    	admin.deleteFTable(orgTable);
	    }
	    admin.createFTable(orgTable, orgTableDescriptor);
	    
	    MTableDescriptor tableDescriptor = new MTableDescriptor(MTableType.UNORDERED);
	    tableDescriptor.setSchema(new Schema(new String[]{"ID"}));
	    if (admin.existsMTable(mTable)) {
	        admin.deleteMTable(mTable);
	    }
	    admin.createMTable(mTable, tableDescriptor);
	}
	
	@Override
	public void tearDown(){
		client.close();
	}
	
	protected int nextRandom(int max) {
        return ThreadLocalRandom.current().nextInt(max);
    }
}
